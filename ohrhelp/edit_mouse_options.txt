These options allow you to enable built-in mouse support features.

If you are doing your own mouse control with plotscripts, you may want to disable some of these.
