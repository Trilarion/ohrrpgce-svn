This is a minimal OHRRPGCE game player binary for Linux.
It is mainly intended for packaging Linux distributions of OHRRPGCE
games from non-Linux platforms like Windows and Mac OS X.
