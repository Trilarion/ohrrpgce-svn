This is a minimal OHRRPGCE game player app bundle for Mac OS X.
It only works on Intel Macs. It is mainly intended for packaging
Mac OS X distributions of OHRRPGCE games on non-Mac platforms
like Windows and Linux
